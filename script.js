let allUsers = [];
$.getJSON("https://jsonplaceholder.typicode.com/users", function (users) {
    $.getJSON("https://jsonplaceholder.typicode.com/posts", function (posts) {
        allUsers = users;
        const viseblPosts = posts.map((post) => {
                const card = createCard(post, allUsers);
            return card
            });
            const postList = document.getElementById('twitter-posts');
            postList.append(...viseblPosts);
        })
    });
    const twitter = document.getElementById('twitter-posts');
    twitter.insertAdjacentHTML('beforeend', `<button class="button" id="addPost" data-target="#myModal">Add post</button>`);
    document.body.insertAdjacentHTML('beforeend', `<div id="myModal" class="modal fade">
                <form id="formAddPost">
                <span class="delete">&times;</span>
                <h4 class="modal-title" id="myModalLabel"><p><b>headline:</b><br><input type="text" name="postTitle" placeholder="postTitle" size="40"></p></h4>
                <div class="modal-body"><p><b>text:</b><br><input type="text" size="40" name="postBody" placeholder="postBody"></p></div>
                <input type="submit" value="Создать запись"></form>
                </form>
                </div>`);
    $("#myModal").on("click", ".delete", function () {
        $(this).closest('.modal').removeClass('show');
    });
    $('#formAddPost').on('submit', function (e) {
        e.preventDefault();
        const title = $(this).find('[name="postTitle"]').val();
        const body = $(this).find('[name="postBody"]').val();
        const data = {userId: 1, title, body};
        const form = this;
        $.ajax({
            url: `https://jsonplaceholder.typicode.com/posts`,
            method: 'POST',
            data: JSON.stringify(data),
            dataType: 'json',
            success: function (result) {
                const [resultInfo] = Object.keys(result);
                const newPost = JSON.parse(resultInfo);
              newPost.id = result.id;
              const post = createCard(newPost, allUsers);
                const postList = document.getElementById('twitter-posts');
                postList.prepend(post);
             $(form).find('[name="postTitle"]').val('');
             $(form).find('[name="postBody"]').val('');
                $(form).closest('.modal').removeClass('show');
            }
        })
    });
    $('#addPost').on('click', function () {
        const {target} = this.dataset;
        $(target).addClass('show');
    });
function createCard({userId, title, body, id}, users) {
    const card = document.createElement('div');
    card.className = 'card';
    const {name, username, email} = users.find(({id}) => id === userId);
    card.insertAdjacentHTML('beforeend', `<span class="delete-card">&times;</span>
                                                        <h5>Name: ${name}</br> Username: ${username}</br> Email: ${email}</h5>
                                                                <div class="post-body"><h3 class="title">Title: ${title}.</br> </h3><p>${body}</p>
                                                                <i class="fa fa-edit edit-post"></i></div>`);
    $(card).on('click', '.delete-card', function () {
        $.ajax({
            url: `https://jsonplaceholder.typicode.com/posts/${id}`,
            method: 'DELETE',
            dataType: 'json',
            success: function (data) {
                card.remove();
            }
        });
    });
    $(card).on('click', '.edit-post', function () {
        $(this).closest('.post-body').html(`<form class="form-edit-post">
                                                <div><input type="text" name="post-title" value="${title}"></div>
                                                <div><input type="text" name="post-body" value="${body}"></div>
                                                <div><input type="submit" value="save"></div>
                                                </form>`)
    });
    $(card).on('submit', '.form-edit-post', function (e) {
        e.preventDefault();
        const title = $(this).find('[name="post-title"]').val();
        const body = $(this).find('[name="post-body"]').val();
        const data = {title, body};
        const form = this;
        $.ajax({
            url: `https://jsonplaceholder.typicode.com/posts/${id}`,
            method: 'PUT',
            data: JSON.stringify(data),
            dataType: 'json',
            success: function (result) {
                const [resultInfo] = Object.keys(result);
                const newPost = JSON.parse(resultInfo);
                $(form).closest('.post-body').html(`<div class="post-body"><h3 class="title">Title: ${newPost.title}.</br> </h3><p>${newPost.body}</p>
                                                                <i class="fa fa-edit edit-post"></i></div>`)
            }
        });
    });
    return card
}

